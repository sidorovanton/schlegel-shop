<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Мебельная компания");
?>
    <div class="content">
        <div class="promo-holder">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/local/include/promo_top.php"
                )
            ); ?>
            <div class="container">
                <div class="promo-inner clearfix">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:feedback",
                        ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "EMAIL_TO" => "belickov.vlad1997@yandex.ru",
                            "EVENT_MESSAGE_ID" => array(
                                0 => "9",
                            ),
                            "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                            "REQUIRED_FIELDS" => array(
                                0 => "NAME",
                                1 => "EMAIL",
                                2 => "MESSAGE",
                            ),
                            "USE_CAPTCHA" => "N"
                        ),
                        false
                    ); ?>
                    <div class="promo-box">
                        <div class="counter-block">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:news",
                                "last_news",
                                Array(
                                    "ADD_ELEMENT_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "BROWSER_TITLE" => "-",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "CHECK_DATES" => "Y",
                                    "COMPONENT_TEMPLATE" => "last_news",
                                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                                    "DETAIL_FIELD_CODE" => array(0 => "", 1 => "",),
                                    "DETAIL_PAGER_SHOW_ALL" => "Y",
                                    "DETAIL_PAGER_TEMPLATE" => "",
                                    "DETAIL_PAGER_TITLE" => "Страница",
                                    "DETAIL_PROPERTY_CODE" => array(0 => "", 1 => "",),
                                    "DETAIL_SET_CANONICAL_URL" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "IBLOCK_ID" => "1",
                                    "IBLOCK_TYPE" => "news",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "LIST_FIELD_CODE" => array(0 => "", 1 => "",),
                                    "LIST_PROPERTY_CODE" => array(0 => "", 1 => "",),
                                    "MESSAGE_404" => "",
                                    "META_DESCRIPTION" => "-",
                                    "META_KEYWORDS" => "-",
                                    "NEWS_COUNT" => "1",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_TITLE" => "Новости",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "SEF_MODE" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "Y",
                                    "SHOW_404" => "N",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_ORDER2" => "ASC",
                                    "USE_CATEGORIES" => "N",
                                    "USE_FILTER" => "N",
                                    "USE_PERMISSIONS" => "N",
                                    "USE_RATING" => "N",
                                    "USE_RSS" => "N",
                                    "USE_SEARCH" => "N",
                                    "USE_SHARE" => "N",
                                    "VARIABLE_ALIASES" => array("SECTION_ID" => "SECTION_ID", "ELEMENT_ID" => "ELEMENT_ID",)
                                )
                            ); ?>
                        </div>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/include/promo_benifist.php"
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news",
            "circle_news",
            Array(
                "ADD_ELEMENT_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "circle_news",
                "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                "DETAIL_DISPLAY_TOP_PAGER" => "N",
                "DETAIL_FIELD_CODE" => array(0 => "", 1 => "",),
                "DETAIL_PAGER_SHOW_ALL" => "Y",
                "DETAIL_PAGER_TEMPLATE" => "",
                "DETAIL_PAGER_TITLE" => "Страница",
                "DETAIL_PROPERTY_CODE" => array(0 => "", 1 => "",),
                "DETAIL_SET_CANONICAL_URL" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "7",
                "IBLOCK_TYPE" => "news",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "LIST_FIELD_CODE" => array(0 => "", 1 => "",),
                "LIST_PROPERTY_CODE" => array(0 => "", 1 => "",),
                "MESSAGE_404" => "",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PREVIEW_TRUNCATE_LEN" => "",
                "SEF_MODE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "USE_CATEGORIES" => "N",
                "USE_FILTER" => "N",
                "USE_PERMISSIONS" => "N",
                "USE_RATING" => "N",
                "USE_RSS" => "N",
                "USE_SEARCH" => "N",
                "USE_SHARE" => "N",
                "VARIABLE_ALIASES" => array("SECTION_ID" => "SECTION_ID", "ELEMENT_ID" => "ELEMENT_ID",)
            )
        ); ?>
        <div class="text-box" id="about">
            <div class="container">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/mini_logo.php"
                    )
                ); ?><? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/about.php"
                    )
                ); ?>
            </div>
        </div>
        <div class="catalog-section" id="catalog">
            <div class="lght-white">
            </div>
            <div class="container">
                <span class="title-section">ЗАВОД НЕМЕЦКИХ УПЛОТНИТЕЛЕЙ SCHLEGEL Q-LON В ГЕРМАНИИ</span>
                <span class="catalog-phones">Телефон: +7 (916) 288-58-52 +7 (985) 685-35-15  Получить консультацию эксперта</span>

                <? $APPLICATION->IncludeComponent(
                    "bitrix:catalog",
                    "catalog",
                    Array(
                        "ACTION_VARIABLE" => "action",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BASKET_URL" => "/personal/basket.php",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "COMPONENT_TEMPLATE" => "catalog",
                        "DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
                        "DETAIL_BACKGROUND_IMAGE" => "-",
                        "DETAIL_BRAND_USE" => "N",
                        "DETAIL_BROWSER_TITLE" => "-",
                        "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
                        "DETAIL_DETAIL_PICTURE_MODE" => "IMG",
                        "DETAIL_DISPLAY_NAME" => "Y",
                        "DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
                        "DETAIL_META_DESCRIPTION" => "-",
                        "DETAIL_META_KEYWORDS" => "-",
                        "DETAIL_PROPERTY_CODE" => array(0 => "", 1 => "",),
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DETAIL_USE_COMMENTS" => "N",
                        "DETAIL_USE_VOTE_RATING" => "N",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "FILTER_VIEW_MODE" => "VERTICAL",
                        "IBLOCK_ID" => "8",
                        "IBLOCK_TYPE" => "catalog",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "INSTANT_RELOAD" => "N",
                        "LABEL_PROP" => "-",
                        "LINE_ELEMENT_COUNT" => "3",
                        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                        "LINK_IBLOCK_ID" => "",
                        "LINK_IBLOCK_TYPE" => "",
                        "LINK_PROPERTY_SID" => "",
                        "LIST_BROWSER_TITLE" => "-",
                        "LIST_META_DESCRIPTION" => "-",
                        "LIST_META_KEYWORDS" => "-",
                        "LIST_PROPERTY_CODE" => array(0 => "", 1 => "",),
                        "MESSAGE_404" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_COMPARE" => "Сравнение",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => "30",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array(0 => "PRODOPTCENA",),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRICE_VAT_SHOW_VALUE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPERTIES" => array(),
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "",
                        "SECTIONS_SHOW_PARENT_NAME" => "Y",
                        "SECTIONS_VIEW_MODE" => "LIST",
                        "SECTION_BACKGROUND_IMAGE" => "-",
                        "SECTION_COUNT_ELEMENTS" => "Y",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_TOP_DEPTH" => "2",
                        "SEF_MODE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SHOW_DEACTIVATED" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_TOP_ELEMENTS" => "Y",
                        "SIDEBAR_DETAIL_SHOW" => "Y",
                        "SIDEBAR_PATH" => "",
                        "SIDEBAR_SECTION_SHOW" => "Y",
                        "TEMPLATE_THEME" => "blue",
                        "TOP_ELEMENT_COUNT" => "9",
                        "TOP_ELEMENT_SORT_FIELD" => "sort",
                        "TOP_ELEMENT_SORT_FIELD2" => "id",
                        "TOP_ELEMENT_SORT_ORDER" => "asc",
                        "TOP_ELEMENT_SORT_ORDER2" => "desc",
                        "TOP_LINE_ELEMENT_COUNT" => "3",
                        "TOP_PROPERTY_CODE" => array(0 => "ARTNUMBER", 1 => "PRODBOOL", 2 => "PRODOPTCENA", 3 => "PRODZAKAZ", 4 => "PRODFABR", 5 => "PRODROZNICA", 6 => "PRODCOLOR", 7 => "",),
                        "TOP_VIEW_MODE" => "SECTION",
                        "USE_COMPARE" => "N",
                        "USE_ELEMENT_COUNTER" => "Y",
                        "USE_FILTER" => "N",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "USE_STORE" => "N",
                        "VARIABLE_ALIASES" => array("ELEMENT_ID" => "ELEMENT_ID", "SECTION_ID" => "SECTION_ID",)
                    )
                ); ?>
            </div>
            <span class="info-text">Впервые уплотнители Schlegel Q-Lon доступны в розницу от 20 м.п. Спешите наполнить свой дом теплом и уютом!</span>
        </div>
        <div class="fast-installation" id="installation">
            <div class="container">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/mini_logo.php"
                    )
                ); ?> <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/installation.php"
                    )
                ); ?>
                <div class="installation-contact">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/include/expert_consultation.php"
                        )
                    ); ?>
                </div>
            </div>
        </div>
        <div class="video-about">
            <div class="container">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/mini_logo.php"
                    )
                ); ?> <span class="title-section">ВИДЕОРОЛИКИ ОБ УПЛОТНИТЕЛЯХ SCHLEGEL Q-LON</span>
                <div class="video-slider clearfix">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "slide",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y",
                            "PATH" => "/local/include/video_slider/index.php"
                        ),
                        false
                    ); ?>
                </div>
            </div>
        </div>
        <div class="effects" id="effect">
            <div class="lght-white">
            </div>
            <span class="title-section">КАКОЙ ЭФФЕКТ ВЫ ПОЛУЧАЕТЕ ПРИ ИСПОЛЬЗОВАНИИ УПЛОТНИТЕЛЕЙ Schlegel Q-Lon </span>
            <div class="container">
                <div class="effects-inner">
                    <div class="effects-box">
                        <img src="/local/templates/schlegel-shop/images/home-5-icon-64.png" alt="">
                        <div class="effects-info">
                            <span class="effects-title">Крепкое здоровье.</span>
                            <p>
                                Окна с уплотнителем Schlegel поддерживают иммунитет. Вы избавитесь от: сквозняков,
                                холода и
                                попадания уличной пыли, со стороны улицы
                            </p>
                        </div>
                    </div>
                    <div class="effects-box">
                        <img src="/local/templates/schlegel-shop/images/thumb-6-icon-64.png" alt="">
                        <div class="effects-info">
                            <span class="effects-title">Активность и энергия.</span>
                            <p>
                                Быть активным значит обладать крепким здоровьем. Устанавливайте уплотнители Schlegel и
                                получайте в подарок спокойный сон и легкое пробуждение, повышение работоспособности и
                                жизненного тонуса, а значит, дополнительную энергию для продуктивной жизни.
                            </p>
                        </div>
                    </div>
                    <div class="effects-box">
                        <img src="/local/templates/schlegel-shop/images/coin-10-icon-64.png" alt="">
                        <div class="effects-info">
                            <span class="effects-title"><span id="u1019-2">Семейный бюджет.</span></span>
                            <p>
                                Один раз и на долго! Установив, уплотнитель Schlegel вы можете жить без замены
                                уплотнения
                                окна более 10 лет. При этом Вы больше не будете тратить свой бюджет на смазку для
                                резинового
                                уплотнения и моющие средства.
                            </p>
                        </div>
                    </div>
                    <div class="effects-box">
                        <img src="/local/templates/schlegel-shop/images/smiley-2-icon-64.png" alt="">
                        <div class="effects-info">
                            <span class="effects-title">Хорошее настроение.</span>
                            <p>
                                Когда борьба со сквозняками и шумом, наконец будет закончена, Вы сможете больше уделять
                                внимания своей семье и своим увлечениям.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="effects-form">
                    <? $APPLICATION->IncludeComponent(
	"bitrix:feedback", 
	"feedback_inline", 
	array(
		"COMPONENT_TEMPLATE" => "feedback_inline",
		"EMAIL_TO" => "belickov.vlad1997@yandex.ru",
		"EVENT_MESSAGE_ID" => array(
			0 => "10",
		),
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"USE_CAPTCHA" => "N"
	),
	false
); ?>
                </div>
            </div>
        </div>
        <div class="feedback-slider" id="feedback">
            <div class="container">
                <span class="title-section">ПОЧИТАЙТЕ ОТЗЫВЫ НАШИХ КЛИЕНТОВ</span> <span class="catalog-phones">Более 20000 довольных клиентов по всей России!</span>
                <div class="news-slider">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "slide",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y",
                            "PATH" => "/local/include/feedback_slider/index.php"
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <div class="effects-form">
                <div class="container">
                    <? $APPLICATION->IncludeComponent(
	"bitrix:feedback", 
	"feedback_inline", 
	array(
		"COMPONENT_TEMPLATE" => "feedback_inline",
		"EMAIL_TO" => "belickov.vlad1997@yandex.ru",
		"EVENT_MESSAGE_ID" => array(
			0 => "10",
		),
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"USE_CAPTCHA" => "N"
	),
	false
); ?>
                </div>
            </div>
        </div>
        <div class="deliver" id="buy-section">
            <div class="container">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/mini_logo.php"
                    )
                ); ?> <span class="title-section">ДОСТАВКА И ОПЛАТА</span>
                <div class="deliver-inner">
                    <div class="deliver-box">
                        <div class="circle-numbers">
                            <p>
                                1
                            </p>
                        </div>
                        <div class="deliver-text">
                            <p>
                                Вы оставляете заявку на сайте или звоните по телефону <br>
                                +7 (916) 288-58-52<br>
                                +7 (985) 685-35-15
                            </p>
                        </div>
                    </div>
                    <div class="deliver-box">
                        <div class="circle-numbers">
                            <p>
                                2
                            </p>
                        </div>
                        <div class="deliver-text">
                            <p>
                                Мы свяжемся с Вами в течение рабочего дня, уточняем состав заказа, удобную дату и время,
                                а
                                также адрес доставки
                            </p>
                        </div>
                    </div>
                    <div class="deliver-box">
                        <div class="circle-numbers">
                            <p>
                                3
                            </p>
                        </div>
                        <div class="deliver-text">
                            <p>
                                Заказ формируется в течение трёх рабочих дней<br>
                                Водитель-экспедитор доставляет заказ до Вашей двери или Вы забираете заказ из офиса сами
                            </p>
                        </div>
                    </div>
                    <div class="deliver-box">
                        <div class="circle-numbers">
                            <p>
                                4
                            </p>
                        </div>
                        <div class="deliver-text">
                            <p>
                                Возможна оплата наличными на месте или безналичный расчет
                            </p>
                        </div>
                    </div>
                </div>
                <div class="deliver-price">
                    <div class="delivery-holder">
                        <div class="deliver-text-first">
                            <p>
                                Предоставляем самые выгодные условия доставки:
                            </p>
                            <p>
                                по Москве — 400 рублей; по МО (до 30 км от МКАД) — 400р. +35 рублей/км.
                            </p>
                        </div>
                        <div class="deliver-text-second">
                            <p>
                                Отгружаем по всей России. Более 80 городов. Сотрудничаем только с ведущими транспортными
                                компаниями,
                            </p>
                            <p>
                                такими как Главдоставка, Деловые Линии, ПЭК
                            </p>
                        </div>
                    </div>
                    <button class="send-bt">Заказать</button>
                </div>
            </div>
        </div>
        <div class="new-properties" id="properties">
            <div class="container">
                <span class="title-section">НОВЫЕ СВОЙСТВА НОВЫЕ ВОЗМОЖНОСТИ УПЛОТНИТЕЛЕЙ SCHLEGEL Q-LON</span>
                <div class="new-properties-list">
                    <div class="properties-bg">
                    </div>
                    <ul>
                        <li>Цветовые решения: черный, серый и белый</li>
                        <li>Эстетичный внешний вид</li>
                        <li>Высочайшая теплоизоляция, коэффициент теплопередачи: 0,033 W/mk</li>
                        <li>Высокая звукоизоляция</li>
                        <li>Быстрая установка, без зарезания уплотнителя на углах</li>
                        <li>Не требует смазки и ухода на протяжении всего срока эксплуатации</li>
                        <li>Без запаха</li>
                        <li>Не изменяет цвет</li>
                        <li>Полезный срок эксплуатации более 10 лет</li>
                        <li>Изменение линейных размеров под воздействием температуры - мене 0,5%!</li>
                        <li>Усилие сжатия до рабочего состояния около 15 H, при 25% сжатии на 100мм</li>
                        <li>Противостоит ливню и ветру: отсутсвие протекания при давлении ветра в 600Ра</li>
                        <li>Облегчение усилия при закрывании (эластичность), малое трение</li>
                        <li>Высокая эластичность материала сохраняется в диапазоне температур: +70°С до -60° (не дубеет
                            на
                            морозе), а значит не будет продуваний и промерзаний
                        </li>
                        <li>При испытаниях на трение, скольжение выдержал более 285 тыс. циклов открывания/закрывания
                            без
                            повреждений поверхности
                        </li>
                        <li>Поверхность уплотнителя хорошо моется и очищается от загрязнения, а также выдерживает
                            чистящие
                            средства вплоть до нитрорастворов, что особо актуально для уплотнителей белого цвета
                        </li>
                        <li>Остаточная деформация: &lt; 15%, через ½ час после снятия нагрузки (50% сжатие при 70
                            град.C, в
                            течение 24 часов - EN ISO 1856)
                        </li>
                    </ul>
                </div>
                <div class="feedback">
                    <img src="/local/templates/schlegel-shop/images/man.png" alt=""> <span class="feedback-text">По всем интересующим вас вопросам вы можете проконсультироваться по телефону</span>
                    <span class="feedback-tell">+7 (916) 288-58-52<br>
				 +7 (985) 685-35-15</span>
                    <div class="big-logo">
                    </div>
                </div>
            </div>
        </div>
        <div class="diagrams">
            <div class="diagrams-title-box">
                <span class="diagram-title-first">Резиновое уплотнение</span> <span class="diagram-title-second">Schlegel Q-Lon</span>
            </div>
            <img src="/local/templates/schlegel-shop/images/epdm_vs_q-lon2.jpg" alt="">
        </div>
        <div class="sertificate">
            <div class="container">
                <span class="title-section">НАШИ СЕРТИФИКАТЫ</span>
                <div class="sertificate-items">
                </div>
            </div>
        </div>
    </div>
    <br>
<?$APPLICATION->SetTitle('Главная');?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>