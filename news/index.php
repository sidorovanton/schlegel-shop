<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/news/css/slick.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/news/css/media.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/news/css/index.css" />
<div class="main">
    <div class="container">
        <div class="title_block">
            <p class="title_content">Новости из мира SCHLEGEL</p>
            <p class="title_underline"><p>
        </div>
        <div class="news-slider">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "slide",
                    "EDIT_TEMPLATE" => "",
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "PATH" => '/local/include/news_slider/index.php'
                ), false);
            ?>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

