$(document).ready(function () {
   $('.order-bt').click(function(event){
        event.preventDefault();
        $('.shadow').fadeIn('slow', 'linear');
        var box = $('.order-holder').fadeIn('slow', 'linear');
        box.show();
        box.css("left", (($(window).width() - box.outerWidth()) / 2) + $(window).scrollLeft() + "px");
        box.css("top", (($(window).height() - box.outerHeight()) / 1.2) + $(window).scrollTop() + "px");
        $('input.articul').val($(this).parent().find('.more-bt').attr('href').slice(1));

	});
	$('.close-form').click(function(event){
		event.preventDefault();
        $('.shadow').fadeOut('slow', 'linear');
        $('.order-holder').fadeOut('slow', 'linear');
	});
    $('.show-menu').click(function (e) {
        e.preventDefault();
        if($(this).parents('.header').find('.top_menu_box').css('display') == 'none'){
            $(this).parents('.header').find('.top_menu_box').fadeIn();
            $(this).addClass('active');
        }
        else {
            $(this).parents('.header').find('.top_menu_box').fadeOut();
            $(this).removeClass('active');
        }
    });
    $(".top_menu_box li a.anchor").click(function (e) {
        e.preventDefault();
        if ($(window).width() > 767) {
            elementClick = $(this).attr("href");
            destination = $(elementClick).offset().top-157;
            $("body,html").animate({scrollTop: destination }, 800);
        }
        else {
            elementClick = $(this).attr("href");
            destination = $(elementClick).offset().top-0;
            $("body,html").animate({scrollTop: destination }, 800);
        }
    });
});