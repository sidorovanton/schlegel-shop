<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
       <div class="footer">
            <div class="container">
                <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/include/footer_info.php"
	)
);?> 
                <div class="footer_social_01">
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?144"></script>

                    <!-- VK Widget -->
                    <div id="vk_groups"></div>
                    <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 3, width: "250"}, 87757043);
                    </script>
                    <!--/VK Widget -->
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">(function(w,doc) {
    if (!w.__utlWdgt ) {
        w.__utlWdgt = true;
        var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
    }})(window,document);
    </script>
    <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="disable" data-share-style="1" data-mode="share" data-like-text-enable="false" data-hover-effect="scale" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="fixed-left" data-text-color="#000000" data-share-shape="round" data-sn-ids="fb.vk.tw.ok.em." data-share-size="40" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1489230" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="false" class="uptolike-buttons" ></div>
    <div class="shadow"></div>
<?$APPLICATION->IncludeComponent(
	"bitrix:product.feedback", 
	".default", 
	array(
		"EMAIL_TO" => "belickov.vlad1997@yandex.ru",
		"EVENT_MESSAGE_ID" => array(
			0 => "9",
		),
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
		),
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/slick.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript">
      $(function () {
        $('.catalog-buttons .more-bt').magnificPopup({
          type: 'inline',
          preloader: false,
          focus: '#username',
          modal: true
        });
        $(document).on('click', '.popup-modal-dismiss', function (e) {
          e.preventDefault();
          $.magnificPopup.close();
        });
      });
    </script>
    <script>
        $('.video-slider').slick({
          dots: false,
          arrows: true,
          infinite: true,
          speed: 1000,
          slidesToShow: 2,
          autoplay: false,
          pauseOnFocus: false,
          responsive: [
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1
              }
            }
          ]
        });
        $('.news-slider').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 1000,
          arrows: true,
          dots:false,
          infinite: true,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                adaptiveHeight: true
              }
            }
          ]
       });
    </script>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter28539581 = new Ya.Metrika({id:28539581,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28539581" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
  </body>
</html>