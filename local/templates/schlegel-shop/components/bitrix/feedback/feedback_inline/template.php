<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>


			<!--	
				  <div class="effects-form">
                        <form class="form-check" id="widgetu1290" method="post" enctype="multipart/form-data" action="scripts/form-u1290.php">
                            <input type="text" class="form-name" name="custom_U1291" placeholder="Ваше имя:">
                            <input type="tel" class="form-phone" name="custom_U1327" placeholder="Ваш телефон:">
                            <input type="email" class="form-email" name="Email" placeholder="Ваш email">
                            <button class="btn_submit" id="u1301-17">Отправить</button>
                        </form>
                    </div>
                </div>
				-->
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
<!--action="scripts/form-u1971.php"  -->
  <form  class="form-check" method="post" action="<?=POST_FORM_ACTION_URI?>">
<?=bitrix_sessid_post()?>
		<input type="text" class="form-name" name="user_name" placeholder="<?=GetMessage('MFT_NAME');?>">
		<input type="tel"  class="form-phone" name="phone" placeholder="<?=GetMessage('MFT_PHONE');?>">
		<input type="email"class="form-email"  name="user_email" placeholder="<?=GetMessage('MFT_EMAIL');?>">
		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
		<input class="btn_submit" type = 'submit' name="submit" id="u1301-17" value="<?=GetMessage("MFT_SUBMIT")?>">
</form>