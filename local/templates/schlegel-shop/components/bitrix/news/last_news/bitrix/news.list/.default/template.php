<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$replace = array('#SITE_DIR#' => $SERVER['NAME'],
					'#ID#' => $arResult['ITEMS'][0]['ID']);
					?>
<div class="clock-title"><?=GetMessage('LAST_NEWS')?></div>
    <div class="clock">
	<span class="title-box"><?=$arResult['ITEMS'][0]['NAME']?></span>
	<?=$arResult['ITEMS'][0]['PREVIEW_TEXT']?>
	 <div class="more-holder">
	<a href = "<?=str_replace( array_keys($replace), $replace,$arResult['DETAIL_PAGE_URL'])?>" class="more-bt">Подробнее</a>
	</div>
</div>
