<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $skuTemplate */
/** @var array $templateData */
global $APPLICATION;
?>
<div class="catalog-list">
    <?
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arItemIDs = array(
            'ID' => $strMainID,
            'PICT' => $strMainID . '_pict',
            'SECOND_PICT' => $strMainID . '_secondpict',
            'MAIN_PROPS' => $strMainID . '_main_props',

            'QUANTITY' => $strMainID . '_quantity',
            'QUANTITY_DOWN' => $strMainID . '_quant_down',
            'QUANTITY_UP' => $strMainID . '_quant_up',
            'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
            'BUY_LINK' => $strMainID . '_buy_link',
            'BASKET_ACTIONS' => $strMainID . '_basket_actions',
            'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
            'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
            'COMPARE_LINK' => $strMainID . '_compare_link',

            'PRICE' => $strMainID . '_price',
            'DSC_PERC' => $strMainID . '_dsc_perc',
            'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

            'PROP_DIV' => $strMainID . '_sku_tree',
            'PROP' => $strMainID . '_prop_',
            'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
            'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
        );

        $strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
        $productTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
            ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
            : $arItem['NAME']
        );
        $imgTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
            ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
            : $arItem['NAME']
        );

        $minPrice = false;
        if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
            $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
        ?>
        <div class="catalog-item">
            <div class="clip_frame grpelem logo_product">
                <img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="" width="250" height="250">
                <span class="product-text"><? echo $arItem['PREVIEW_TEXT']; ?></span>

            </div>


            <?
            $articul = $arItem['DISPLAY_PROPERTIES']['ARTNUMBER']['VALUE'];
            ?>
            <p>Артикул <?= $articul; ?> </p>


            <div class="catalog-buttons">
                <a href="javascript:void(0)" class="order-bt">Заказать</a>
                <a href="#<?= $articul; ?>" class="more-bt">Подробнее</a>
            </div>

            <div id="<?= $articul; ?>" class="mfp-hide white-popup-block zoom-anim-dialog">
                <img src=" <?= SITE_TEMPLATE_PATH ?>/images/<?= strtolower($articul); ?>.jpg" alt="" width="499"
                     height="316">
                <div class="more-content">
                    <p>Schlegel Q-Lon</p>
                    <p>Арт. <?= $articul ?></p>
                    <p>Для деревянных окон и дверей</p>
                    <p>Цвет: <?= $arItem['DISPLAY_PROPERTIES']['PRODCOLOR']['VALUE'] ?></p>
                    <p>Производитель: <?= $arItem['DISPLAY_PROPERTIES']['PRODFABR']['VALUE'] ?></p>

                    <p>Наличие: <?= $arItem['DISPLAY_PROPERTIES']['PRODBOOL']['VALUE'] ?></p>

                    <p class="retail">Розница: <?= $arItem['DISPLAY_PROPERTIES']['PRODROZNICA']['VALUE'] ?>
                        руб. м.п.</p>
                    <p class="retail">Оптовая: <?= $arItem['DISPLAY_PROPERTIES']['PRODOPTCENA']['VALUE'] ?> руб.
                        м.п.</p>
                    <? if ($arItem['DISPLAY_PROPERTIES']['PRODZAKAZ']['VALUE'] == 1) {
                        ?>
                        <p>Под заказ</p>
                        <?
                    } ?>

                </div>
                <a class="popup-modal-dismiss" href="#">Закрыть</a>
            </div>
        </div>
        <?
    }
    ?>
</div>
