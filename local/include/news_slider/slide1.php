<div>
    <div class="info-block">
        <div class="top-part">
            <img src="<?=SITE_TEMPLATE_PATH?>/news/images/slide-img-1.jpg" alt="">
            <div class="title-box">
                <span class="news-title">АКЦИЯ от Schlegel</span>
                <span class="news-info">Дата: 01 Мая 2017 года</span>
            </div>
        </div>
        <div class="news-text">
            <p>Компания Schlegel предоставляет Вам скидку на Уплотнитель для пластиковых окон КВЕ, Rehau,
                Proplex, Montblanc, Exprof, Novotex, Kaleva, Brusbox, Grain, Faust и пр. Артикул QL-9646 в размере 20%.
                <br><br>
                Данная скидка предоставляется в течении всего Апреля месяца (с 01 Мая 2017 года до 31 Мая 2017 года).
                <br><br>
                Подробности можно узнать позвонив нам по телефону, написав письмо по электронной почте или в онлайн чате на нашем сайте.
            </p>
        </div>
    </div>
    <div class="info-block">
        <div class="top-part">
            <img src="<?=SITE_TEMPLATE_PATH?>/news/images/slide-img-1.jpg" alt="">
            <div class="title-box">
                <span class="news-title">АКЦИЯ от Schlegel</span>
                <span class="news-info">Дата: 01 Апреля 2017 года</span>
            </div>
        </div>
        <div class="news-text">
            <p>Компания Schlegel предоставляет Вам скидку на Уплотнитель для пластиковых окон КВЕ, Rehau,
                Proplex, Montblanc, Exprof, Novotex, Kaleva, Brusbox, Grain, Faust и пр. Артикул QL-9646 в размере 20%.
                <br><br>
                Данная скидка предоставляется в течении всего Апреля месяца (с 01 Апреля 2017 года до 30 Апреля 2017 года).
                <br><br>
                Подробности можно узнать позвонив нам по телефону, написав письмо по электронной почте или в онлайн чате на нашем сайте.
            </p>
        </div>
    </div>

    <div class="info-block">
        <div class="top-part">
            <img src="<?=SITE_TEMPLATE_PATH?>/news/images/slide-img-2.jpg" alt="">
            <div class="title-box">
                <span class="news-title">Компания Schlegel приобретает компанию Giesse</span>
                <span class="news-info">Дата: 26 Марта 2016</span>
            </div>
        </div>
        <div class="news-text">
            <p>Представители компании Schlegel International, специализирующейся на производстве уплотнителей, объявили о приобретении итальянского поставщика фурнитурной продукции, компании Giesse, с незамедлительным вступлением в силу.
                <br><br>
                Это позволит компании Schlegel, международному подразделению компании Tyman plc, акции которой котируются на Лондонской фондовой бирже, осуществлять поставки высококачественной фурнитурной продукции на европейские рынки алюминиевых окон и дверей, а также обеспечить локальное присутствие на новых мировых рынках.</p>
        </div>
    </div>
    <div class="info-block">
        <div class="top-part">
            <img src="<?=SITE_TEMPLATE_PATH?>/news/images/slide-img-3.jpg" alt="">
            <div class="title-box">
                <span class="news-title">Новые упаковочные решения Schlegel</span>
                <span class="news-info">Дата: 26 Марта 2016 года</span>
            </div>
        </div>
        <div class="news-text">
            <p>На протяжении долгих лет компания Schlegel внедряла новые типы и конструкции упаковки, не выводя из обращения предыдущие виды упаковки, подлежащие замене.
                <br><br>
                В этом году мы намерены заменить существующие упаковочные решения на всех производственных площадках Schlegel:
                <br><br>
                Новое внешнее оформление в соответствии с корпоративным стилем;<br>
                Стандартизация и оптимизация размеров ящиков и рулонов для перевозки на поддонах и удобства хранения на складе;<br>
                Более эффективная защита и наматывание наших изделий;<br>
                Указания по транспортировке и хранению на ящиках (например, «Верх»);<br>
                Указание размеров ящиков на днище.

            </p>
        </div>
    </div>
    <div class="info-block">
        <div class="top-part">
            <img src="<?=SITE_TEMPLATE_PATH?>/news/images/slide-img-4.jpg" alt="">
            <div class="title-box">
                <span class="news-title">Сопротивление ветру сильнее со Schlegel</span>
                <span class="news-info">Дата: 02 Декабря 2015 года</span>
            </div>
        </div>
        <div class="news-text">
            <p>Schlegel сам разрабатывает, изготавливает и выполняет сборку своих изделий. Основной целью является обеспечение максимальной прозрачности. По этой причине окна, двери и стеклянные конструкции с уплотнителем Schlege могут быть оснащены самыми тонкими профилями из возможных.
                <br><br>
                Поскольку требования к погодоустойчивости звучат все чаще, для оптимальной герметизации помещений компания Metaglas мы использует продукцию компании Schlegel, в том числе уплотнения Q-LON. Это позволяет им изготавливать для своих клиентов раздвижные конструкции, которые сохраняют высокую ветро и влагоустойчивость.</p>
        </div>
    </div>
</div>