<span class="title-section">БЫСТРЫЙ МОНТАЖ НЕМЕЦКИХ УПЛОТНИТЕЛЕЙ SCHLEGEL Q-LON ДЛЯ ОКОН И ДВЕРЕЙ</span>
                    <span class="catalog-phones">Окно без продуваний и промерзаний за 10 мин в любое время года!</span>
                    <div class="installation-box">
                        <div class="installation-photo">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/552.jpg" alt="">
                        </div>
                        <div class="installation-instruction">
                            <div>
                                <span class="instruction-title">Инструкция по установке оконного уплотнителя:</span>
                                <div class="installation-list">
                                    <ul>
                                        <li>1. Демонтировать старый уплотнитель из оконной створки или рамы и утилизировать его.</li>
                                        <li>2.  Зачистить или протереть освобожденные отверстия от пыли и всевозможных загрязнителей.</li>
                                        <li>3. Установка уплотнителя осуществляется сверху вниз. Шов должен находиться наверху створки посередине.</li>
                                        <li>4. Размещаем в оконный паз новый уплотнитель Schlegel Q-Lon, по всему периметру оконной рамы или створки, не вытягивая его в углах.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>