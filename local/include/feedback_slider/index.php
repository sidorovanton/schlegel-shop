<?php
$dirPath = '/local/include/feedback_slider/';
$dir = $_SERVER['DOCUMENT_ROOT'].$dirPath;
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if($file != 'index.php' && $file != '.' && $file != '..')
			$APPLICATION->IncludeComponent(
						"bitrix:main.include", 
						".default", 
						array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "slide",
							"EDIT_TEMPLATE" => "",
							"COMPONENT_TEMPLATE" => ".default",
							"AREA_FILE_RECURSIVE" => "Y",
							"PATH" => $dirPath.$file
						),false);
        }
        closedir($dh);
    }
}
?>