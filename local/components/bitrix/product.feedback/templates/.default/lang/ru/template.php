<?
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "Ваш Email";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_COMMENT'] = "Вы можете оставить комментарий или указать интересующий Вас артикул";
$MESS ['MFT_PHONE'] = "Ваш телефон";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
?>