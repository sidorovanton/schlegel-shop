<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

?>

<div class="order-holder">
    <a href="javascript:void(0)" class="close-form">Закрыть</a>
    <? if (!empty($arResult["ERROR_MESSAGE"])) {
        foreach ($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
    }
    if (strlen($arResult["OK_MESSAGE"]) > 0) {
        ?>
        <div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div><?
    }

    ?>

    <form action="<?= POST_FORM_ACTION_URI ?>" method="POST">
        <span class="form-title"><span>Оставьте Вашу заявку</span></span>
        <?= bitrix_sessid_post() ?>

        <input type="text" class="form-input" name="user_name" placeholder="<?= GetMessage('MFT_NAME'); ?>">
        <input type="email" class="form-input" name="user_email" placeholder="<?= GetMessage('MFT_EMAIL'); ?>">
        <input type="tel" class="form-input" name="phone" placeholder="<?= GetMessage('MFT_PHONE'); ?>">
        <textarea cols="30" class="form-area" rows="10" name="user_comment"
                  placeholder="<?= GetMessage('MFT_COMMENT'); ?>"></textarea>
        <input type="hidden" class="articul" name="articul" value="">
        <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
        <input type="submit" class="btn_submit" id="u1967-17" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
    </form>
</div>