<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="promo-form">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
<span>Оставьте заявку на уплотнитель Schlegel и мы свяжемся с Вами в ближайшее время!</span>
<?=bitrix_sessid_post()?>
		<input type="text" class="form-input" name="user_name" placeholder="<?=GetMessage('MFT_NAME');?>">
		<input type="text"  class="form-input" name="user_email" placeholder="<?=GetMessage('MFT_EMAIL');?>">
		<input type="tel" class="form-input" name="phone" placeholder="<?=GetMessage('MFT_PHONE');?>">
		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
		<input type="submit" class="send-bt" id="u105-17" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
</form>
</div>